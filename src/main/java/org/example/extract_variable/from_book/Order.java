package org.example.extract_variable.from_book;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Order {


  private double quantity;
  private double itemPrice;
}
