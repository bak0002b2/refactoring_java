package org.example.extract_variable.from_book;

public class Before {

  public double calculateOrder(Order order) {

    return order.getQuantity() * order.getItemPrice() -
        Math.max(0, order.getQuantity() - 500) * order.getItemPrice() * 0.05 +
        Math.min(order.getQuantity() * order.getItemPrice() * 0.1, 100);
  }

}
