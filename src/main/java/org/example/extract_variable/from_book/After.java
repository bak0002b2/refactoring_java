package org.example.extract_variable.from_book;

public class After {

  public double calculateOrder(Order order) {

    var basePrice = order.getQuantity() * order.getItemPrice();
    var  quantityDiscount = Math.max(0, order.getQuantity() - 500) * order.getItemPrice();
    var shipping = Math.min(basePrice * 0.1, 100);

    return basePrice - quantityDiscount + shipping;

  }

}
