package org.example.parametrize_function.from_book;

public class Before {

  public void fivePercentRaise(Person aPerson) {
      aPerson.multiplySalary(1.1F);
  }
  public  void tenPercentRaise(Person aPerson) {
    aPerson.multiplySalary(1.05F);
  }
}
