package org.example.parametrize_function.from_book;

public class After {

  public void raise(Person aPerson, float raise) {
    aPerson.multiplySalary(raise);
  }
}
