package org.example.parametrize_function.from_book;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Person {

  private Float salary;

  public void multiplySalary(float factor) {
    this.salary = this.salary * factor;
  }

}
