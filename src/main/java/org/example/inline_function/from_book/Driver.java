package org.example.inline_function.from_book;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Driver {

  private int numberOfLateDeliveries;
}
