package org.example.inline_function.from_book;

public class After {
  public int rating(Driver aDriver) {
    return aDriver.getNumberOfLateDeliveries() > 5 ? 2 : 1;
  }

}
