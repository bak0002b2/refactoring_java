package org.example.inline_function.from_book;


public class Before {
  public int rating(Driver aDriver) {
    return moreThanFiveLateDeliveries(aDriver) ? 2 : 1;
  }
  private boolean moreThanFiveLateDeliveries(Driver aDriver) {
    return aDriver.getNumberOfLateDeliveries() > 5;
  }
}
