package org.example.inline_function.from_code_base.before;


import lombok.extern.slf4j.Slf4j;
import org.example.inline_function.from_book.Driver;

@Slf4j
public class DriverService {

  private DriverRepository driverRepository;

  public Driver saveClip(Driver driver) {
    return saveDriver(driver);
  }

  public Driver saveDriver(Driver driver) {

    log.info("Saving");

    var savedDriver = this.driverRepository.save(driver);

    return savedDriver;
  }

}
