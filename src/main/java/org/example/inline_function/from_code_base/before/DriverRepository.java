package org.example.inline_function.from_code_base.before;

import org.example.inline_function.from_book.Driver;

public interface DriverRepository {

  public Driver save(Driver driver);

}
