package org.example.inline_function.from_code_base.after;


import lombok.extern.slf4j.Slf4j;
import org.example.inline_function.from_book.Driver;
import org.example.inline_function.from_code_base.before.DriverRepository;

@Slf4j
public class DriverService {
  private DriverRepository driverRepository;

  public Driver saveClip(Driver driver) {
    log.info("Saving");
    return this.driverRepository.save(driver);
  }
}
