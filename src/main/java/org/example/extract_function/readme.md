# Idea
Extract fragment of code into its own function named after its purpose.

# Motivation
* Short function are easier to read
* You can now the responsibility of the method without reading the code

# Related clean code principles
* SRP


