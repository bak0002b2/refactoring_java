# From Catalog
The patterns and code example below are taken from Martin Fowler's book. You can check the 
website clicking [here](https://refactoring.com/catalog/)

1. [Extract Function](/src/main/java/org/example/extract_function/readme.md)
2. [Inline Function](/src/main/java/org/example/inline_function/readme.md)
3. [Extract Variable](/src/main/java/org/example/extract_variable/readme.md)
4. [Inline Variable](/src/main/java/org/example/inline_variable/readme.md)
5. [Parametrize Function](/src/main/java/org/example/parametrize_function/readme.md)

# Other resources
* [Refactoring guru](https://refactoring.guru/refactoring/techniques)